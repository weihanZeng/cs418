/**
 * @file A simple WebGL example for viewing meshes read from OBJ files
 * @author Eric Shaffer <shaffer1@illinois.edu>
 * @author Eric Roch <emroch2@illinois.edu>
 */

// Environment parameters
/** @global The WebGL context */
var gl;
/** @global The HTML5 canvas we draw on */
var canvas;
/** @global A simple GLSL shader program */
var shaderProgram;

// Rendering parameters
/** @global The Modelview matrix */
var mvMatrix = mat4.create();
/** @global The View matrix */
var vMatrix = mat4.create();
/** @global The Rotation matrix */
var rMatrix = mat4.create();
var rMatrixInv = mat4.create();
/** @global The Projection matrix */
var pMatrix = mat4.create();
/** @global The Normal matrix */
var nMatrix = mat3.create();
/** @global The matrix stack for hierarchical modeling */
var mvMatrixStack = [];

// View parameters
/** @global Location of the camera in world coordinates */
var eyePt = vec3.fromValues(0.0,0.0,3.0);
/** @global Location of a point along viewDir in world coordinates */
var viewPt = vec3.fromValues(0.0,0.0,0.0);
/** @global Direction of the view in world coordinates */
var viewDir = vec3.fromValues(0.0,0.0,-1.0);
/** @global Up vector for view matrix creation, in world coordinates */
var up = vec3.fromValues(0.0,1.0,0.0);
/** @global Pitch axis for view matrix rotation, in world coordinates */
var pitchAxis = vec3.fromValues(1.0,0.0,0.0);

// Light parameters
/** @global Light position in VIEW coordinates */
var lightPosition = [1,1,1];
/** @global Ambient light color/intensity for Phong reflection */
var lAmbient = [0,0,0];
/** @global Diffuse light color/intensity for Phong reflection */
var lDiffuse = [1,1,1];
/** @global Specular light color/intensity for Phong reflection */
var lSpecular =[0.5,0.5,0.5];

// Material parameters
/** @global Ambient material color/intensity for Phong reflection */
var kAmbient = [1.0,1.0,1.0];
/** @global Diffuse material color/intensity for Phong reflection */
var kDiffuse = [205.0/255.0,163.0/255.0,63.0/255.0];
/** @global Specular material color/intensity for Phong reflection */
var kSpecular = [1.0,1.0,1.0];
/** @global Shininess exponent for Phong reflection */
var shininess = 10;
/** @global Edge color fpr wireframeish rendering */
var kEdgeBlack = [0.0,0.0,0.0];
/** @global Edge color for wireframe rendering */
var kEdgeWhite = [1.0,1.0,1.0];

// Model parameters
/** An object holding the geometry for a 3D mesh */
var myMesh;
/** An object holding a texture cube */
var mySkyBox;
var skyboxScale = 6;    // size of rendered skybox

var keysDown = [];

const PI = Math.PI;
const TWO_PI = 2*Math.PI;
const HALF_PI = 0.5*Math.PI;

// camera position
var rho = 3;    // radius
var theta = 0;  // elevation (angle from XZ plane towards +Y)
var phi = 0;    // azimuth (angle from +Z towards +X)
var dir = 1;    // does up == up or down?

// model rotation
var modelX = vec3.fromValues(1,0,0);
var modelY = vec3.fromValues(0,1,0);
var modelZ = vec3.fromValues(0,0,-1);

var rotSpeed = 30;  // deg/sec
var lastFrame = -1;


//----------------------------------------------------------------------------------
// GLSL Communication Functions:
//----------------------------------------------------------------------------------

/**
* Sends material information to the shader
* @param {Float32} alpha shininess coefficient
* @param {Float32Array} a Ambient material color
* @param {Float32Array} d Diffuse material color
* @param {Float32Array} s Specular material color
*/
function setMaterialUniforms(alpha,a,d,s) {
    gl.uniform1f(shaderProgram.uniformShininessLoc, alpha);
    gl.uniform3fv(shaderProgram.uniformAmbientMaterialColorLoc, a);
    gl.uniform3fv(shaderProgram.uniformDiffuseMaterialColorLoc, d);
    gl.uniform3fv(shaderProgram.uniformSpecularMaterialColorLoc, s);
}

/**
* Sends light information to the shader
* @param {Float32Array} loc Location of light source
* @param {Float32Array} a Ambient light strength
* @param {Float32Array} d Diffuse light strength
* @param {Float32Array} s Specular light strength
*/
function setLightUniforms(loc,a,d,s) {
    gl.uniform3fv(shaderProgram.uniformLightPositionLoc, loc);
    gl.uniform3fv(shaderProgram.uniformAmbientLightColorLoc, a);
    gl.uniform3fv(shaderProgram.uniformDiffuseLightColorLoc, d);
    gl.uniform3fv(shaderProgram.uniformSpecularLightColorLoc, s);
}

/**
* Sends projection/modelview matrices to shader
*/
function setMatrixUniforms() {
    uploadModelViewMatrixToShader();
    uploadNormalMatrixToShader();
    uploadProjectionMatrixToShader();
}

/**
 * Sends Modelview matrix to shader
 */
function uploadModelViewMatrixToShader() {
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
    gl.uniformMatrix4fv(shaderProgram.rMatrixInvUniform, false, rMatrixInv);
}

/**
 * Sends projection matrix to shader
 */
function uploadProjectionMatrixToShader() {
    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
}

/**
 * Generates and sends the normal matrix to the shader
 */
function uploadNormalMatrixToShader() {
    mat3.fromMat4(nMatrix,mvMatrix);
    mat3.transpose(nMatrix,nMatrix);
    mat3.invert(nMatrix,nMatrix);
    gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, nMatrix);
}


//----------------------------------------------------------------------------------
// WebGL Initialization Functions
//----------------------------------------------------------------------------------

/**
 * Creates a context for WebGL
 * @param {element} canvas WebGL canvas
 * @return {Object} WebGL context
 */
function createGLContext(canvas) {
    var names = ["webgl", "experimental-webgl"];
    var context = null;
    for (var i=0; i < names.length; i++) {
        try {
            context = canvas.getContext(names[i]);
        }
        catch(e) {}

        if (context) {
            break;
        }
    }
    if (context) {
        context.viewportWidth = canvas.width;
        context.viewportHeight = canvas.height;
    }
    else {
        alert("Failed to create WebGL context!");
    }
    return context;
}

/**
 * Loads Shaders
 * @param {string} id ID string for shader to load. Either vertex shader/fragment shader
 */
function loadShaderFromDOM(id) {
    var shaderScript = document.getElementById(id);

    // If we don't find an element with the specified id
    // we do an early exit
    if (!shaderScript) {
        return null;
    }

    // Loop through the children for the found DOM element and
    // build up the shader source code as a string
    var shaderSource = "";
    var currentChild = shaderScript.firstChild;
    while (currentChild) {
        if (currentChild.nodeType == 3) { // 3 corresponds to TEXT_NODE
            shaderSource += currentChild.textContent;
        }
        currentChild = currentChild.nextSibling;
    }

    var shader;
    if (shaderScript.type == "x-shader/x-fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    }
    else if (shaderScript.type == "x-shader/x-vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    }
    else {
        return null;
    }

    gl.shaderSource(shader, shaderSource);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert(gl.getShaderInfoLog(shader));
        return null;
    }
    return shader;
}

/**
 * Setup the fragment and vertex shaders
 */
function setupShaders() {
    vertexShader = loadShaderFromDOM("shader-vs");
    fragmentShader = loadShaderFromDOM("shader-fs");

    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Failed to setup shaders");
    }

    gl.useProgram(shaderProgram);

    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

    shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
    gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
    shaderProgram.nMatrixUniform = gl.getUniformLocation(shaderProgram, "uNMatrix");
    shaderProgram.rMatrixInvUniform = gl.getUniformLocation(shaderProgram, "uRMatrixInv");
    shaderProgram.uniformLightPositionLoc = gl.getUniformLocation(shaderProgram, "uLightPosition");
    shaderProgram.uniformAmbientLightColorLoc = gl.getUniformLocation(shaderProgram, "uAmbientLightColor");
    shaderProgram.uniformDiffuseLightColorLoc = gl.getUniformLocation(shaderProgram, "uDiffuseLightColor");
    shaderProgram.uniformSpecularLightColorLoc = gl.getUniformLocation(shaderProgram, "uSpecularLightColor");
    shaderProgram.uniformShininessLoc = gl.getUniformLocation(shaderProgram, "uShininess");
    shaderProgram.uniformAmbientMaterialColorLoc = gl.getUniformLocation(shaderProgram, "uKAmbient");
    shaderProgram.uniformDiffuseMaterialColorLoc = gl.getUniformLocation(shaderProgram, "uKDiffuse");
    shaderProgram.uniformSpecularMaterialColorLoc = gl.getUniformLocation(shaderProgram, "uKSpecular");
}


//----------------------------------------------------------------------------------
// Utility Functions
//----------------------------------------------------------------------------------

/**
* Pushes matrix onto modelview matrix stack
*/
function mvPushMatrix() {
    var copy = mat4.clone(mvMatrix);
    mvMatrixStack.push(copy);
}

/**
* Pops matrix off of modelview matrix stack
*/
function mvPopMatrix() {
    if (mvMatrixStack.length == 0) {
        throw "Invalid popMatrix!";
    }
    mvMatrix = mvMatrixStack.pop();
}

/**
* Translates degrees to radians
* @param {Number} degrees Degree input to function
* @return {Number} The radians that correspond to the degree input
*/
function deg2rad(degrees) {
    return degrees * Math.PI / 180;
}

/**
 * Adjusts the camera (eye) position according to the global
 * spherical coordinate variables rho, theta, and phi.  Also
 * Adjusts the viewDir vector to point from the camera to the
 * viewPt.
 */
function moveCamera() {
    // convert spherical coordinates to cartesian
    // theta = 0, phi = 0       -> (0,0,1)
    // theta = 0, phi = pi/2    -> (1,0,0)
    // theta = 0, phi = pi      -> (0,0,-1)
    // theta = 0, phi = -pi/2   -> (-1,0,0)
    // theta = pi/2, phi = 0    -> (0,1,0)
    // theta = -pi/2, phi=0     -> (0,-1,0)
    eyePt[0] = rho * Math.cos(theta) * Math.sin(phi);
    eyePt[1] = rho * Math.sin(theta);
    eyePt[2] = rho * Math.cos(theta) * Math.cos(phi);

    // update view direction vector
    viewDir = viewPt - eyePt;
    vec3.normalize(viewDir, viewDir);
}

/**
 * Enabe texture rendering in fragment shader
 */
function enableTexture() {
    gl.uniform1i(gl.getUniformLocation(shaderProgram, "uTextureEnabled"), 1);
}

/**
 * Disable texture rendering in fragment shader
 */
function disableTexture() {
    gl.uniform1i(gl.getUniformLocation(shaderProgram, "uTextureEnabled"), 0);
}


//----------------------------------------------------------------------------------
// MP Specific Functions
//----------------------------------------------------------------------------------

/**
 * Setup the model mesh with data from and OBJ file
 * @param  {String} filename OBJ file with mesh data to load
 */
function setupMesh(filename) {
    myMesh = new TriMesh();
    console.log("Created myMesh");

    myMesh.loadGeometry(filename);
}

/**
 * Setup a skybox with defualt texture images (stored in images/ folder).
 * The SkyBox will span from (-1,-1,-1) to (1,1,1), and can be scaled
 * if a bigger box is desired.
 */
function setupSkyBox() {
    mySkyBox = new SkyBox();
    console.log("Created mySkyBox");

    // Load the default images, stored in the "images" subdirectory of the current folder
    mySkyBox.loadImages();
    // Generate skybox geometry
    mySkyBox.generateCube();
}


//----------------------------------------------------------------------------------
// Drawing/Animation Functions
//----------------------------------------------------------------------------------

/**
* Startup function called from html code to start program.
*/
function startup() {
    canvas = document.getElementById("myGLCanvas");
    gl = createGLContext(canvas);

    checkExtension();
    setupShaders();
    setupMesh("teapot.obj");
    setupSkyBox();

    gl.clearColor(0.8, 0.8, 0.8, 1.0);
    gl.enable(gl.DEPTH_TEST);

    document.onkeydown = handleKeyDown;
    document.onkeyup = handleKeyUp;

    tick();
}

/**
* Check if OES Element Index browser extension is available
*/
function checkExtension() {
    // Get extension for 4 byte integer indices for drawElements
    var ext = gl.getExtension('OES_element_index_uint');
    if (ext == null) {
        alert("OES_element_index_uint is unsupported by your browser and terrain generation cannot proceed.");
    }
    else {
        console.log("OES_element_index_uint is supported!");
    }
}

/**
* Keeping drawing frames....
*/
function tick() {
    requestAnimFrame(tick);
    animate();
    draw();
}

/**
* Update any model transformations
*/
function animate() {
    var now = new Date().getTime();

    if (lastFrame == -1) {
        lastFrame = now;
    }

    var elapsed = (now - lastFrame) / 1000.0;
    lastFrame = now;

    var turnAmount = elapsed * rotSpeed;

    // update pitch, roll, and yaw of model
    var q = quat.create();
    {
        // pitch
        if (keysDown["w"]) {
            quat.setAxisAngle(q, modelX, deg2rad(turnAmount*1.5));
            vec3.transformQuat(modelY, modelY, q);
            vec3.transformQuat(modelZ, modelZ, q);
        }
        else if (keysDown["s"]) {
            quat.setAxisAngle(q, modelX, deg2rad(-turnAmount*1.5));
            vec3.transformQuat(modelY, modelY, q);
            vec3.transformQuat(modelZ, modelZ, q);
        }
        // roll
        if (keysDown["q"]) {
            quat.setAxisAngle(q, modelZ, deg2rad(turnAmount*1.5));
            vec3.transformQuat(modelY, modelY, q);
            vec3.transformQuat(modelX, modelX, q);
        }
        else if (keysDown["e"]) {
            quat.setAxisAngle(q, modelZ, deg2rad(-turnAmount*1.5));
            vec3.transformQuat(modelY, modelY, q);
            vec3.transformQuat(modelX, modelX, q);
        }
        // yaw
        if (keysDown["a"]) {
            quat.setAxisAngle(q, modelY, deg2rad(turnAmount*1.5));
            vec3.transformQuat(modelX, modelX, q);
            vec3.transformQuat(modelZ, modelZ, q);
        }
        else if (keysDown["d"]) {
            quat.setAxisAngle(q, modelY, deg2rad(-turnAmount*1.5));
            vec3.transformQuat(modelX, modelX, q);
            vec3.transformQuat(modelZ, modelZ, q);
        }

        vec3.normalize(modelX, modelX);
        vec3.normalize(modelY, modelY);
        vec3.normalize(modelZ, modelZ);
    }

    // update camera position
    {
        // elevation
        if (keysDown["ArrowUp"]) {
            theta += dir * deg2rad(turnAmount);
        }
        else if (keysDown["ArrowDown"]) {
            theta -= dir * deg2rad(turnAmount);
        }
        // azimuth
        if (keysDown["ArrowLeft"]) {
            phi -= dir * deg2rad(turnAmount);
        }
        else if (keysDown["ArrowRight"]) {
            phi += dir * deg2rad(turnAmount);
        }
        // radius
        if (keysDown["="]) {
            rho -= 1.0 * elapsed;
        }
        else if (keysDown["-"]) {
            rho += 1.0 * elapsed;
        }

        // keep rho in [1,skyboxScale]
        if (rho < 1)            rho = 1;
        if (rho > skyboxScale)  rho = skyboxScale;

        // keep theta in [-pi/2, pi/2], adjusting phi if necessary
        if (theta < -HALF_PI) {
            theta = -(PI + theta);  // adjust elevation
            phi += PI;              // flip azimuth
            up[1] = -up[1];         // we are now upside down
            dir = -dir;             // up now means down and down means up
        }
        if (theta > HALF_PI) {
            theta = PI - theta;     // adjust elevation
            phi += PI;              // flip azimuth
            up[1] = -up[1];         // we are now upside down
            dir = -dir;             // up now means down and down means up
        }
        // keep phi in [-pi, pi)
        if (phi < -PI) {
            phi += TWO_PI;          // we crossed the "international date line"
        }
        if (phi >= PI) {
            phi -= TWO_PI;          // we crossed the "international date line"
        }

        // console.log("Theta: ", theta);
        // console.log("Phi: ", phi);
        // console.log("Up: ", up);

        moveCamera();
    }
}

/**
 * Draw call that applies matrix transformations to model and draws model in frame
 */
function draw() {
    //console.log("function draw()")

    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // We'll use perspective
    mat4.perspective(pMatrix,deg2rad(60),
                     gl.viewportWidth / gl.viewportHeight,
                     0.1, 500.0);

    // // We want to look down -z, so create a lookat point in that direction
    // vec3.add(viewPt, eyePt, viewDir);

    // Then generate the lookat matrix and initialize the view matrix to that view
    mat4.lookAt(vMatrix, eyePt, viewPt, up);
    mat4.lookAt(rMatrix, [0,0,0], modelZ, modelY);

    // Draw Mesh
    if (myMesh.loaded()) {
        mvPushMatrix();

        mat4.multiply(mvMatrix, rMatrix, mvMatrix);
        mat4.multiply(mvMatrix, vMatrix, mvMatrix);

        mat4.invert(rMatrixInv, rMatrix);
        // vec3.transformMat4(lightPosition, lightPosition, vMatrix);

        setMatrixUniforms();
        setLightUniforms(lightPosition, lAmbient, lDiffuse, lSpecular);
        setMaterialUniforms(shininess, kAmbient, kDiffuse, kSpecular);

        if (mySkyBox.loaded() && document.getElementById("reflection").checked)
            enableTexture();

        myMesh.drawTriangles();
        disableTexture();

        mvPopMatrix();
    }

    // Draw Skybox
    if (mySkyBox.loaded() && document.getElementById("skybox").checked) {
        mvPushMatrix();

        mat4.scale(mvMatrix, mvMatrix, vec3.fromValues(skyboxScale,skyboxScale,skyboxScale));
        mat4.multiply(mvMatrix, vMatrix, mvMatrix);

        setMatrixUniforms();
        setLightUniforms(lightPosition, lAmbient, lDiffuse, lSpecular);

        enableTexture();
        mySkyBox.drawCube();
        disableTexture();

        mvPopMatrix();
    }
}

//----------------------------------------------------------------------------------
// Code to handle user interaction

function handleKeyDown(event) {
    // console.log("Key down ", event.key, " code ", event.code);
    keysDown[event.key] = true;

    if (keysDown["ArrowUp"] || keysDown["ArrowDown"] ||
        keysDown["ArrowLeft"] || keysDown["ArrowRight"]) {
        event.preventDefault();
    }

    // Reset position
    if (keysDown["0"]) {
        rho = 3;        // distance = 3
        theta = 0;      // we are on the "equator"
        phi = 0;        // we are on the "prime maridian"
        up[1] = 1.0;    // +Y is up
        dir = 1;        // up means up
    }
    if (keysDown["r"]) {
        modelX = vec3.fromValues(1,0,0);
        modelY = vec3.fromValues(0,1,0);
        modelZ = vec3.fromValues(0,0,-1);
    }
}

function handleKeyUp(event) {
    // console.log("Key up ", event.key, " code ", event.code);
    keysDown[event.key] = false;
}
