/** @global The WebGL context */
var gl;

/** @global The HTML5 canvas we draw on */
var canvas;

/** @global A simple GLSL shader program */
var shaderProgram;

/** @global The WebGL buffer holding the vertices */
var vertexPositionBuffer;

/** @global The WebGL buffer holding the vertex colors */
var vertexColorBuffer;

/** @global The WebGL buffer holding the mesh indices */
var vertexIndexBuffer;

/** @global The Modelview matrix */
var mvMatrix = mat4.create();

/** @global The Projection matrix */
var pMatrix = mat4.create();

/** @global Angle for non-affine transformation **/
var defAngle = 0.0;

/** @global Amplitude for non-affine transformation **/
var defAmpl = 30.0;

/** @global Angle for affine rotation **/
var rotAngle = 0.0;


//==========================================================================//
//    WebGL Utility Functions                                               //
//==========================================================================//

/**
 * Creates a context for WebGL
 * @param {element} canvas WebGL canvas
 * @return {Object} WebGL context
 */
function createGLContext(canvas) {
    var names = ["webgl", "experimental-webgl"];
    var context = null;
    for (var i=0; i < names.length; i++) {
        try {
            context = canvas.getContext(names[i]);
        } catch(e) {}
        if (context) {
            break;
        }
    }
    if (context) {
        context.viewportWidth = canvas.width;
        context.viewportHeight = canvas.height;
    } else {
        alert("Failed to create WebGL context!");
    }
    return context;
}

/**
 * Loads Shaders
 * @param {string} id ID string for shader to load. Either vertex shader/fragment shader
 */
function loadShaderFromDOM(id) {
    var shaderScript = document.getElementById(id);

    // If we don't find an element with the specified id
    // we do an early exit
    if (!shaderScript) {
        return null;
    }

    // Loop through the children for the found DOM element and
    // build up the shader source code as a string
    var shaderSource = "";
    var currentChild = shaderScript.firstChild;
    while (currentChild) {
        if (currentChild.nodeType == 3) { // 3 corresponds to TEXT_NODE
            shaderSource += currentChild.textContent;
        }
        currentChild = currentChild.nextSibling;
    }

    var shader;
    if (shaderScript.type == "x-shader/x-fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (shaderScript.type == "x-shader/x-vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
        return null;
    }

    gl.shaderSource(shader, shaderSource);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert(gl.getShaderInfoLog(shader));
        return null;
    }
    return shader;
}

/**
 * Setup the fragment and vertex shaders
 */
function setupShaders() {
    vertexShader = loadShaderFromDOM("shader-vs");
    fragmentShader = loadShaderFromDOM("shader-fs");

    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Failed to setup shaders");
    }

    gl.useProgram(shaderProgram);
    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

    shaderProgram.vertexColorAttribute = gl.getAttribLocation(shaderProgram, "aVertexColor");
    gl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);

    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
}

/**
 * Sends projection/modelview matrices to shader
 */
function setMatrixUniforms() {
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
}


//==========================================================================//
//    Generic Utility Functions                                             //
//==========================================================================//

/**
 * Convert an angle from degrees to radians.
 * @param  {Number} degrees Input angle, in degrees
 * @return {Number}         Output angle, in radians
 */
function deg2rad(degrees) {
    return degrees * Math.PI / 180;
}


//==========================================================================//
//    Application Specific Functions                                        //
//==========================================================================//

/**
 * Startup function called from html code to start program.
 */
function startup() {
    canvas = document.getElementById("myGLCanvas");
    gl = createGLContext(canvas);
    setupShaders();
    setupBuffers();
    gl.clearColor(1.0, 1.0, 1.0, 1.0);
    gl.enable(gl.DEPTH_TEST);
    tick();
}

/**
 * Populate buffers with data
 */
function setupBuffers() {
    loadVertices();     // Generate the vertex positions
    loadColors();       // Generate the vertex colors
    loadFaces();        // Generate the triangle mesh
}

/**
 * Tick called for every animation frame.
 */
function tick() {
    requestAnimFrame(tick);
    draw();
    animate();
}

/**
 * Draw call that applies matrix transformations to model and draws model in frame
 */
function draw() {
    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Reset the model view and projection matrices
    mat4.identity(mvMatrix);
    mat4.identity(pMatrix);

    // Set the projection to an orthographic projection of the unit cube,
    // stretched in the z-axis to avoid clipping
    mat4.ortho(pMatrix,-1,1,-1,1,2,-2);

    // Apply transformations to the model view matrix to move the image.
    // - We will start by scaling the model from model coordinates (0-1000)
    //   to the range 0-2. Note that we must also invert the y-axis because
    //   the model points were defined with the origin at the top.
    // - To get window coordinates, we must still translate the image to put
    //   the origin at the center.  This requires a shift of -1 along x and
    //   +1 along y (after the inversion).
    // - Next, we will rotate the image around the axis defined by y=x, z=y/2
    {
        var angle = deg2rad(rotAngle);
        mat4.rotate(mvMatrix, mvMatrix, angle, vec3.fromValues(1.0, 1.0, 0.5));
        mat4.translate(mvMatrix, mvMatrix, vec3.fromValues(-1, 1, 0));
        mat4.scale(mvMatrix, mvMatrix, vec3.fromValues(0.002, -0.002, 1.0));
    }

    console.log(mvMatrix);

    // send the updated matrices to WebGL
    setMatrixUniforms();

    // tell WebGL how to extract vertex positions
    {
        const itemSize = vertexPositionBuffer.itemSize;
        const type = gl.FLOAT;
        const normalize = false;
        const stride = 0;
        const offset = 0;
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexPositionBuffer);
        gl.vertexAttribPointer(
            shaderProgram.vertexPositionAttribute,
            itemSize,
            type,
            normalize,
            stride,
            offset);
    }

    // tell WebGL how to extract vertex colors
    {
        const itemSize = vertexColorBuffer.itemSize;
        const type = gl.FLOAT;
        const normalize = false;
        const stride = 0;
        const offset = 0;
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexColorBuffer);
        gl.vertexAttribPointer(
            shaderProgram.vertexColorAttribute,
            itemSize,
            type,
            normalize,
            stride,
            offset);
    }

    // tell WebGL how to index the vertices
    {
        const itemCount = 90;
        const type = gl.UNSIGNED_SHORT;
        const offset = 0;
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vertexIndexBuffer);
        gl.drawElements(gl.TRIANGLES, itemCount, type, offset);
    }
}

/**
 * Animation to be called from tick. Updates globals and performs animation for each tick.
 */
function animate() {
    // perform animations
    defAngle += 2.5;        // oscillate with frequency 2.5 deg / frame
    if (defAngle > 360) {   // keep the angle in the range [0, 360]
        defAngle -= 360;
    }

    rotAngle += 0.5;        // rotate the model 0.5 degrees per frame

    loadVertices();
}

/**
 * Populate vertex buffer with data
 */
function loadVertices() {
    var angle = deg2rad(defAngle);

    // Indexed vertex list - these verticies describe the corners of the
    // Fighting Illini Badge, in counterclockwise order from the top left
    // followed by the orange flags from left to right.  The vertexIndexBuffer
    // is used to create a triangle mesh from this list, rather than
    // duplicating each of the vertices here.
    const positions = [
        85,  60,  0,    // blue badge
        85,  205, 0,
        170, 205, 0,
        170, 640, 0,
        350, 640, 0,
        350, 525, 0,
        415, 525, 0,
        415, 325, 0,
        350, 325, 0,
        350, 205, 0,
        650, 205, 0,
        650, 325, 0,
        585, 325, 0,
        585, 525, 0,
        650, 525, 0,
        650, 640, 0,
        830, 640, 0,
        830, 205, 0,
        915, 205, 0,
        915, 60,  0,

        // The orange flags will wiggle up and down following a sine wave.
        // The y-coordinates are adjusted as a function of the x-coordinate
        // and a time-varying deformation angle (defAngle).  This gives
        // the effect of a travelling sine wave, as the computed angle
        // changes with time and is proportional to the x position.
        170, 670 + defAmpl * Math.sin(170 - angle), 0,    // flag 1
        170, 730 - defAmpl * Math.sin(170 - angle), 0,
        230, 765 - defAmpl * Math.sin(230 - angle), 0,
        230, 670 + defAmpl * Math.sin(230 - angle), 0,

        290, 670 + defAmpl * Math.sin(290 - angle), 0,    // flag 2
        290, 805 - defAmpl * Math.sin(290 - angle), 0,
        350, 840 - defAmpl * Math.sin(350 - angle), 0,
        350, 670 + defAmpl * Math.sin(350 - angle), 0,

        410, 670 + defAmpl * Math.sin(410 - angle), 0,    // flag 3
        410, 880 - defAmpl * Math.sin(410 - angle), 0,
        470, 915 - defAmpl * Math.sin(470 - angle), 0,
        470, 670 + defAmpl * Math.sin(470 - angle), 0,

        530, 670 + defAmpl * Math.sin(530 - angle), 0,    // flag 4
        530, 915 - defAmpl * Math.sin(530 - angle), 0,
        590, 880 - defAmpl * Math.sin(590 - angle), 0,
        590, 670 + defAmpl * Math.sin(590 - angle), 0,

        650, 670 + defAmpl * Math.sin(650 - angle), 0,    // flag 5
        650, 840 - defAmpl * Math.sin(650 - angle), 0,
        710, 805 - defAmpl * Math.sin(710 - angle), 0,
        710, 670 + defAmpl * Math.sin(710 - angle), 0,

        770, 670 + defAmpl * Math.sin(770 - angle), 0,    // flag 6
        770, 765 - defAmpl * Math.sin(770 - angle), 0,
        830, 730 - defAmpl * Math.sin(830 - angle), 0,
        830, 670 + defAmpl * Math.sin(830 - angle), 0
    ];

    // Create a WebGL buffer and fill it with the vertex data.  The buffer's
    // usage parameter is set to gl.DYNAMIC_DRAW because we will be updating
    // the vertex positions each frame.
    vertexPositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexPositionBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.DYNAMIC_DRAW);

    // Set some additional attributes for the size of each item in the buffer
    // (3D vertices), and the number of items (total length / item size)
    vertexPositionBuffer.itemSize = 3;
    vertexPositionBuffer.numItems = positions.length / 3;

    console.log(positions);
}

/**
 * Populate color buffer with data
 */
function loadColors() {
    // Define some RBGA color objects which we will use
    // to fill the vertex color array.
    const blue = { r:0.086, g:0.157, b:0.298, a:1.0 };
    const orange = { r:0.910, g:0.290, b:0.212, a:1.0 };

    var colors = [];

    // Set the first 20 vertices to blue (the upper badge)
    for (i = 0; i < 20; i++) {
        colors.push(blue.r);
        colors.push(blue.g);
        colors.push(blue.b);
        colors.push(blue.a);
    }
    // Set the remaining vertices to orange (the pennants)
    for (i = 20; i < 44; i++) {
        colors.push(orange.r);
        colors.push(orange.g);
        colors.push(orange.b);
        colors.push(orange.a);
    }

    // Create a WebGL buffer and fill it with the vertex color data.
    // The buffer's usage parameter is set to gl.STATIC_DRAW because we
    // will not be changing the colors of the vertices dynamically.
    vertexColorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexColorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

    // Set some additional attributes for the size of each item in the buffer
    // (4 channel colors), and the number of items (total length / item size)
    vertexColorBuffer.itemSize = 4;
    vertexColorBuffer.numItems = colors.length / 4;

    console.log(colors);
}

/**
 * Populate element buffer with data
 */
function loadFaces() {
    // Create a list of vertex indices that will define the triangle mesh.
    // Each set of three indices corresponds to a triangle in the mesh,
    // and each index corresponds to that element of the vertexPositionBuffer.
    const indices = [
        0,  1,  2,      0,  2,  19,     // top bar
        2,  9,  19,     9,  10, 19,
        10, 17, 19,     17, 18, 19,
        2,  3,  9,      3,  8,  9,      // left pillar
        3,  5,  8,      3,  4,  5,
        5,  6,  7,      5,  7,  8,      // left insert
        10, 16, 17,     10, 11, 16,     // right pillar
        11, 14, 16,     14, 15, 16,
        11, 12, 13,     11, 13, 14,     // right insert

        20, 21, 22,     20, 22, 23,
        24, 25, 26,     24, 26, 27,
        28, 29, 30,     28, 30, 31,
        32, 33, 34,     32, 34, 35,
        36, 37, 38,     36, 38, 39,
        40, 41, 42,     40, 42, 43
    ];

    // Create a WebGL buffer and fill it with the index data. We will bind this to
    // the gl.ELEMENT_ARRAY_BUFFER to tell WebGL that this is an index list instead
    // of vertex data.
    vertexIndexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vertexIndexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

    // Set some additional attributes for the size of each item in the buffer
    // (3 vertices per triangle), and the number of items (total length / item size)
    vertexIndexBuffer.itemSize = 3;
    vertexIndexBuffer.numItems = indices.length / 3;

    console.log(indices);
}
