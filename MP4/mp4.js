/**
 * @file A simple WebGL example for viewing meshes read from OBJ files
 * @author Eric Shaffer <shaffer1@illinois.edu>
 * @author Eric Roch <emroch2@illinois.edu>
 */

// Environment parameters
/** @global The WebGL context */
var gl;
/** @global The HTML5 canvas we draw on */
var canvas;
/** @global A simple GLSL shader program */
var shaderProgram;

// Rendering parameters
/** @global The Modelview matrix */
var mvMatrix = mat4.create();
/** @global The View matrix */
var vMatrix = mat4.create();
/** @global The Projection matrix */
var pMatrix = mat4.create();
/** @global The Normal matrix */
var nMatrix = mat3.create();
/** @global The matrix stack for hierarchical modeling */
var mvMatrixStack = [];

// View parameters
/** @global Location of the camera in world coordinates */
var eyePt = vec3.fromValues(0.0,0.0,3.0);
/** @global Location of a point along viewDir in world coordinates */
var viewPt = vec3.fromValues(0.0,0.0,0.0);
/** @global Direction of the view in world coordinates */
var viewDir = vec3.fromValues(0.0,0.0,-1.0);
/** @global Up vector for view matrix creation, in world coordinates */
var up = vec3.fromValues(0.0,1.0,0.0);

// Light parameters
/** @global Light position in VIEW coordinates */
var lightPosition = [1,1,1];
/** @global Ambient light color/intensity for Phong reflection */
var lAmbient = [0.3,0.3,0.3];
/** @global Diffuse light color/intensity for Phong reflection */
var lDiffuse = [1,1,1];
/** @global Specular light color/intensity for Phong reflection */
var lSpecular =[0.5,0.5,0.5];

// Material parameters
/** @global Ambient material color/intensity for Phong reflection */
var kAmbient = [1.0,1.0,1.0];
/** @global Diffuse material color/intensity for Phong reflection */
var kDiffuse = [205.0/255.0,163.0/255.0,63.0/255.0];
/** @global Specular material color/intensity for Phong reflection */
var kSpecular = [0.5,0.5,0.5];
/** @global Shininess exponent for Phong reflection */
var shininess = 4;
/** @global Edge color fpr wireframeish rendering */
var kEdgeBlack = [0.0,0.0,0.0];
/** @global Edge color for wireframe rendering */
var kEdgeWhite = [1.0,1.0,1.0];


var particle_list = [];
var drag = 0.15;
var gravity = [0,-10,0];

var sphereVertexPositionBuffer;
var sphereVertexNormalBuffer;

var planeVertexPositionBuffer;

var lastFrame = -1;

//----------------------------------------------------------------------------------
// GLSL Communication Functions:
//----------------------------------------------------------------------------------

/**
* Sends material information to the shader
* @param {Float32} alpha shininess coefficient
* @param {Float32Array} a Ambient material color
* @param {Float32Array} d Diffuse material color
* @param {Float32Array} s Specular material color
*/
function setMaterialUniforms(alpha,a,d,s) {
    gl.uniform1f(shaderProgram.uniformShininessLoc, alpha);
    gl.uniform3fv(shaderProgram.uniformAmbientMaterialColorLoc, a);
    gl.uniform3fv(shaderProgram.uniformDiffuseMaterialColorLoc, d);
    gl.uniform3fv(shaderProgram.uniformSpecularMaterialColorLoc, s);
}

/**
* Sends light information to the shader
* @param {Float32Array} loc Location of light source
* @param {Float32Array} a Ambient light strength
* @param {Float32Array} d Diffuse light strength
* @param {Float32Array} s Specular light strength
*/
function setLightUniforms(loc,a,d,s) {
    gl.uniform3fv(shaderProgram.uniformLightPositionLoc, loc);
    gl.uniform3fv(shaderProgram.uniformAmbientLightColorLoc, a);
    gl.uniform3fv(shaderProgram.uniformDiffuseLightColorLoc, d);
    gl.uniform3fv(shaderProgram.uniformSpecularLightColorLoc, s);
}

/**
* Sends projection/modelview matrices to shader
*/
function setMatrixUniforms() {
    uploadModelViewMatrixToShader();
    uploadNormalMatrixToShader();
    uploadProjectionMatrixToShader();
}

/**
 * Sends Modelview matrix to shader
 */
function uploadModelViewMatrixToShader() {
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}

/**
 * Sends projection matrix to shader
 */
function uploadProjectionMatrixToShader() {
    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
}

/**
 * Generates and sends the normal matrix to the shader
 */
function uploadNormalMatrixToShader() {
    mat3.fromMat4(nMatrix,mvMatrix);
    mat3.transpose(nMatrix,nMatrix);
    mat3.invert(nMatrix,nMatrix);
    gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, nMatrix);
}

function setShadingMode() {
    var mode = document.getElementById("mode").value;
    console.log(mode);
    if (mode == "none") {
        gl.uniform1i(shaderProgram.uniformShadingMode, 0);
    }
    else if (mode == "phong") {
        gl.uniform1i(shaderProgram.uniformShadingMode, 1);
    }
    else if (mode == "blinn-phong") {
        gl.uniform1i(shaderProgram.uniformShadingMode, 2);
    }
}


//----------------------------------------------------------------------------------
// WebGL Initialization Functions
//----------------------------------------------------------------------------------

/**
 * Creates a context for WebGL
 * @param {element} canvas WebGL canvas
 * @return {Object} WebGL context
 */
function createGLContext(canvas) {
    var names = ["webgl", "experimental-webgl"];
    var context = null;
    for (var i=0; i < names.length; i++) {
        try {
            context = canvas.getContext(names[i]);
        }
        catch(e) {}

        if (context) {
            break;
        }
    }
    if (context) {
        context.viewportWidth = canvas.width;
        context.viewportHeight = canvas.height;
    }
    else {
        alert("Failed to create WebGL context!");
    }
    return context;
}

/**
 * Loads Shaders
 * @param {string} id ID string for shader to load. Either vertex shader/fragment shader
 */
function loadShaderFromDOM(id) {
    var shaderScript = document.getElementById(id);

    // If we don't find an element with the specified id
    // we do an early exit
    if (!shaderScript) {
        return null;
    }

    // Loop through the children for the found DOM element and
    // build up the shader source code as a string
    var shaderSource = "";
    var currentChild = shaderScript.firstChild;
    while (currentChild) {
        if (currentChild.nodeType == 3) { // 3 corresponds to TEXT_NODE
            shaderSource += currentChild.textContent;
        }
        currentChild = currentChild.nextSibling;
    }

    var shader;
    if (shaderScript.type == "x-shader/x-fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    }
    else if (shaderScript.type == "x-shader/x-vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    }
    else {
        return null;
    }

    gl.shaderSource(shader, shaderSource);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert(gl.getShaderInfoLog(shader));
        return null;
    }
    return shader;
}

/**
 * Setup the fragment and vertex shaders
 */
function setupShaders() {
    vertexShader = loadShaderFromDOM("shader-vs");
    fragmentShader = loadShaderFromDOM("shader-fs");

    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Failed to setup shaders");
    }

    gl.useProgram(shaderProgram);

    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

    shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
    gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
    shaderProgram.nMatrixUniform = gl.getUniformLocation(shaderProgram, "uNMatrix");
    shaderProgram.uniformLightPositionLoc = gl.getUniformLocation(shaderProgram, "uLightPosition");
    shaderProgram.uniformAmbientLightColorLoc = gl.getUniformLocation(shaderProgram, "uAmbientLightColor");
    shaderProgram.uniformDiffuseLightColorLoc = gl.getUniformLocation(shaderProgram, "uDiffuseLightColor");
    shaderProgram.uniformSpecularLightColorLoc = gl.getUniformLocation(shaderProgram, "uSpecularLightColor");
    shaderProgram.uniformShininessLoc = gl.getUniformLocation(shaderProgram, "uShininess");
    shaderProgram.uniformAmbientMaterialColorLoc = gl.getUniformLocation(shaderProgram, "uKAmbient");
    shaderProgram.uniformDiffuseMaterialColorLoc = gl.getUniformLocation(shaderProgram, "uKDiffuse");
    shaderProgram.uniformSpecularMaterialColorLoc = gl.getUniformLocation(shaderProgram, "uKSpecular");

    shaderProgram.uniformShadingMode = gl.getUniformLocation(shaderProgram, "uShadingMode");
}


//----------------------------------------------------------------------------------
// Utility Functions
//----------------------------------------------------------------------------------

/**
* Pushes matrix onto modelview matrix stack
*/
function mvPushMatrix() {
    var copy = mat4.clone(mvMatrix);
    mvMatrixStack.push(copy);
}

/**
* Pops matrix off of modelview matrix stack
*/
function mvPopMatrix() {
    if (mvMatrixStack.length == 0) {
        throw "Invalid popMatrix!";
    }
    mvMatrix = mvMatrixStack.pop();
}

/**
* Translates degrees to radians
* @param {Number} degrees Degree input to function
* @return {Number} The radians that correspond to the degree input
*/
function deg2rad(degrees) {
    return degrees * Math.PI / 180;
}

//----------------------------------------------------------------------------------
// MP Specific Functions
//----------------------------------------------------------------------------------

/**
 * Populates buffers with data for spheres
 */
function setupSphereBuffers() {
    var sphereSoup = [];
    var sphereNormals = [];
    var numT = sphereFromSubdivision(6, sphereSoup, sphereNormals);
    // console.log("Generated " + numT + " triangles");

    // fill sphereVertexPositionBuffer with vertex data
    sphereVertexPositionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexPositionBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(sphereSoup), gl.STATIC_DRAW);
    sphereVertexPositionBuffer.itemSize = 3;
    sphereVertexPositionBuffer.numItems = numT*3;
    // console.log("Faces: " + sphereSoup.length/9);

    // fill sphereVertexNormalBuffer with normal data
    sphereVertexNormalBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexNormalBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(sphereNormals), gl.STATIC_DRAW);
    sphereVertexNormalBuffer.itemSize = 3;
    sphereVertexNormalBuffer.numItems = numT*3;
    // console.log("Normals: " + sphereNormals.length/3);
}

function drawSphere() {
    const type = gl.FLOAT;
    const normalize = false;
    const stride = 0;
    const offset = 0;

    // Bind vertex buffer
    gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexPositionBuffer);
    gl.vertexAttribPointer(
        shaderProgram.vertexPositionAttribute,
        sphereVertexPositionBuffer.itemSize,
        type,
        normalize,
        stride,
        offset
    );

    // Bind normal buffer
    gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexNormalBuffer);
    gl.vertexAttribPointer(
        shaderProgram.vertexNormalAttribute,
        sphereVertexNormalBuffer.itemSize,
        type,
        normalize,
        stride,
        offset
    );

    gl.drawArrays(gl.TRIANGLES, 0, sphereVertexPositionBuffer.numItems);
}

function setupPlaneBuffers() {
    var planeSoup = [];
    var numT = planeFromSubdivision(6, -1,1,-1,1,planeSoup);
    planeVertexPositionBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, planeVertexPositionBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(planeSoup), gl.STATIC_DRAW);
    planeVertexPositionBuffer.itemSize = 3;
    planeVertexPositionBuffer.numItems = numT*3;
}

function drawPlane() {
    gl.bindBuffer(gl.ARRAY_BUFFER, planeVertexPositionBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute,
                        planeVertexPositionBuffer.itemSize,
                        gl.FLOAT, false, 0, 0);
    gl.drawArrays(gl.TRIANGLES, 0, planeVertexPositionBuffer.numItems);
}

function addParticles() {
    var count = document.getElementById("count").value;

    for (var i = 0; i < count; i++) {
        var p = new Particle();
        for (var j = 0; j < 3; j++) {
            p.position[j] = Math.random() * 2 - 1;
            p.velocity[j] = Math.random() * 6 - 3;
            p.color[j] = Math.random();
        }
        p.radius = Math.random() * 0.06 + 0.02; // random radius in [0.02,0.08)
        p.acceleration = gravity;

        particle_list.push(p);
    }
    console.log("Added " + count + " particles");
}

function reset() {
    console.log("Reset");
    particle_list = [];
}

//----------------------------------------------------------------------------------
// Drawing/Animation Functions
//----------------------------------------------------------------------------------

/**
* Startup function called from html code to start program.
*/
function startup() {
    canvas = document.getElementById("myGLCanvas");
    gl = createGLContext(canvas);

    checkExtension();
    setupShaders();
    setShadingMode();
    setupSphereBuffers();
    setupPlaneBuffers();

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.enable(gl.DEPTH_TEST);

    document.onkeydown = handleKeyDown;
    document.onkeyup = handleKeyUp;

    document.getElementById("add").onclick = addParticles;
    document.getElementById("reset").onclick = reset;

    addParticles();
    // var p1 = new Particle();
    // var p2 = new Particle();

    // p1.set_position(-0.5,0,0);
    // p1.set_radius(0.05);
    // p1.set_color(1,0,0);
    // p1.set_acceleration(...gravity);

    // p2.set_position(0.5,0,0);
    // p2.set_radius(0.1);
    // p2.set_color(0,1,0);
    // p2.set_acceleration(...gravity);

    // particle_list.push(p1);
    // particle_list.push(p2);

    tick();
}

/**
* Check if OES Element Index browser extension is available
*/
function checkExtension() {
    // Get extension for 4 byte integer indices for drawElements
    var ext = gl.getExtension('OES_element_index_uint');
    if (ext == null) {
        alert("OES_element_index_uint is unsupported by your browser and terrain generation cannot proceed.");
    }
    else {
        console.log("OES_element_index_uint is supported!");
    }
}

/**
* Keeping drawing frames....
*/
function tick() {
    requestAnimFrame(tick);
    // if paused, set the last frame to -1
    // so delta_t isn't massive when resuming
    if (document.getElementById("pause").checked) {
        lastFrame = -1;
    }
    else {
        animate();
    }

    lightPosition[0] = document.getElementById("lightX").value;
    lightPosition[1] = document.getElementById("lightY").value;
    lightPosition[2] = document.getElementById("lightZ").value;

    draw();
}

/**
* Update any model transformations
*/
function animate() {
    var now = new Date().getTime();

    if (lastFrame == -1) {
        lastFrame = now;
    }

    var delta_t = (now - lastFrame) / 1000.0;
    lastFrame = now;

    for (var i = 0; i < particle_list.length; i++) {
        var particle = particle_list[i];

        var p = particle.position;
        var v = particle.velocity;
        var a = particle.acceleration;
        var r = particle.radius;

        // calculate updated velocity using Euler integration
        // with an air resistance component
        v[0] = (v[0] * ((1-drag) ** delta_t)) + (a[0] * delta_t);
        v[1] = (v[1] * ((1-drag) ** delta_t)) + (a[1] * delta_t);
        v[2] = (v[2] * ((1-drag) ** delta_t)) + (a[2] * delta_t);

        // calculate updated position using Euler integration
        p[0] += v[0] * delta_t;
        p[1] += v[1] * delta_t;
        p[2] += v[2] * delta_t;

        // update acceleration in case gravity has changed
        a[0] = gravity[0];
        a[1] = gravity[1];
        a[2] = gravity[2];

        // check for collisions along each axis
        // ... For each axis in p[], check the element value
        // ... against the walls of the cube.  If outside the
        // ... cube, update p[idx] to clamp the value. If the
        // ... position is on a wall and the particle is moving
        // ... towards that wall, reverse the velocity.
        p.forEach(function(pos, idx) {
            if (Math.abs(pos) + r > 1) {
                p[idx] = (1-r) * Math.sign(pos);
                if (Math.sign(pos) == Math.sign(v[idx])) {
                    v[idx] = -v[idx];
                }
            }
        });

        // set velocities to zero if they are close to zero
        v.forEach(function(vel, idx) {
            if (Math.abs(vel) < 0.001) {
                v[idx] = 0;
            }
        });
    }
}

/**
 * Draw call that applies matrix transformations to model and draws model in frame
 */
function draw() {
    //console.log("function draw()")

    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // We'll use perspective
    mat4.perspective(pMatrix,deg2rad(60),
                     gl.viewportWidth / gl.viewportHeight,
                     0.1, 500.0);

    // We want to look down -z, so create a lookat point in that direction
    vec3.add(viewPt, eyePt, viewDir);

    // Then generate the lookat matrix and initialize the view matrix to that view
    mat4.lookAt(mvMatrix, eyePt, viewPt, up);

    setLightUniforms(lightPosition, lAmbient, lDiffuse, lSpecular);

    if (document.getElementById("box").checked) {
        // disable the normal attribute becuase
        // we aren't sending normals for the box
        gl.disableVertexAttribArray(shaderProgram.vertexNormalAttribute);

        // draw bottom plane
        mvPushMatrix();
        mat4.translate(mvMatrix, mvMatrix, [0,-1,0]);
        mat4.rotateX(mvMatrix, mvMatrix, deg2rad(90));
        setMatrixUniforms();
        setMaterialUniforms(shininess, [1,0,0], kDiffuse, kSpecular);
        drawPlane();
        mvPopMatrix();

        // draw top plane
        mvPushMatrix();
        mat4.translate(mvMatrix, mvMatrix, [0,1,0]);
        mat4.rotateX(mvMatrix, mvMatrix, deg2rad(90));
        setMatrixUniforms();
        setMaterialUniforms(shininess, [0,1,0], kDiffuse, kSpecular);
        drawPlane();
        mvPopMatrix();

        // draw left plane
        mvPushMatrix();
        mat4.translate(mvMatrix, mvMatrix, [-1,0,0]);
        mat4.rotateY(mvMatrix, mvMatrix, deg2rad(90));
        setMatrixUniforms();
        setMaterialUniforms(shininess, [0,0,1], kDiffuse, kSpecular);
        drawPlane();
        mvPopMatrix();

        // draw right plane
        mvPushMatrix();
        mat4.translate(mvMatrix, mvMatrix, [1,0,0]);
        mat4.rotateY(mvMatrix, mvMatrix, deg2rad(90));
        setMatrixUniforms();
        setMaterialUniforms(shininess, [1,1,0], kDiffuse, kSpecular);
        drawPlane();
        mvPopMatrix();

        // enable the normal attribute for the spheres
        gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);
    }

    for (let p of particle_list) {
        // console.log(p);
        mvPushMatrix();

        // configure MV matrix for current sphere
        mat4.translate(mvMatrix, mvMatrix, p.position);
        mat4.scale(mvMatrix, mvMatrix, [p.radius, p.radius, p.radius]);

        // send data to shader programs
        setMatrixUniforms();
        setMaterialUniforms(shininess, p.color, p.color, kSpecular);

        // draw sphere
        drawSphere();

        mvPopMatrix();
    }
}

//----------------------------------------------------------------------------------
// Code to handle user interaction
var keysDown = [];

function handleKeyDown(event) {
    // console.log("Key down ", event.key, " code ", event.code);
    keysDown[event.key] = true;

    if (keysDown[" "]) {
        var paused = document.getElementById("pause").checked;
        document.getElementById("pause").checked = !paused;
    }
    if (keysDown["b"]) {
        var drawBox = document.getElementById("box").checked;
        document.getElementById("box").checked = !drawBox;
    }
    if (keysDown["r"]) {
        reset();
    }
    if (keysDown["a"]) {
        addParticles();
    }

    if (keysDown["ArrowUp"] || keysDown["ArrowDown"] ||
        keysDown["ArrowLeft"] || keysDown["ArrowRight"]) {
        event.preventDefault();
    }

    if (keysDown["ArrowUp"]) {
        gravity = [0, 10, 0];
    }
    if (keysDown["ArrowDown"]) {
        gravity = [0, -10, 0];
    }
    if (keysDown["ArrowLeft"]) {
        gravity = [-10, 0, 0];
    }
    if (keysDown["ArrowRight"]) {
        gravity = [10, 0, 0];
    }

}

function handleKeyUp(event) {
    // console.log("Key up ", event.key, " code ", event.code);
    keysDown[event.key] = false;
}
