# Repository for CS 418: Interactive Computer Graphics
### Fall 2018 w/ Professor John Hart

## Course Description

Basic mathematical tools and computational techniques for modeling, rendering, and animating 3-D scenes.

---

## MP Descriptions

### MP 1

Introduction to WebGL and animation.  Model and draw a 2D object, then animate by applying both affine and non-affine transformations.

### MP 2

Part A - Terrain Generation.  Implementing the Diamond-Square terrain generation algorithm to create a realistic terrain.  Apply lighting and color to the scene.

Part B - Flight Simulator.  Implement camera motion to fly over the terrain generated in part A.  Use quaternions to encode camera rotation and keyboard input to control flight.  Also add fog to the scene.

### MP 3

Part A - Environment Maps.  Implement a skybox which renders a scene surrounding the camera and place a model in the center.  Add lighting and camera motion, maintaining proper lighting behavior as the camera moves.

Part B - Evironment Maps (cont).  Add refectance to the model to reflect the surrounding scene, as well as allowing the motion of the model in addition to camera motion.  Reflection and lighting should be consistant as camera and model move.

### MP 4

Simple particle system.  Implementing a basic particle system with multiple balls bouncing in a closed container.  Particles are affected by both gravity and air resistance, and position/velocity are calculated using Euler integration.