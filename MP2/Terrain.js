/**
 * @fileoverview Terrain - A simple 3D terrain using WebGL
 * @author Eric Shaffer
 */

/** Class implementing 3D terrain. */
class Terrain {
    /**
     * Initialize members of a Terrain object
     * @param {number} div Number of triangles along x axis and y axis
     * @param {number} minX Minimum X coordinate value
     * @param {number} maxX Maximum X coordinate value
     * @param {number} minY Minimum Y coordinate value
     * @param {number} maxY Maximum Y coordinate value
     */
    constructor(div, minX, maxX, minY, maxY) {
        this.div  =  div;
        this.minX = minX;
        this.minY = minY;
        this.maxX = maxX;
        this.maxY = maxY;

        this.numVertices = (div+1)*(div+1);
        this.numFaces = 2*div*div;

        // Allocate vertex array
        this.vBuffer = [];
        // Allocate triangle array
        this.fBuffer = [];
        // Allocate normal array
        this.nBuffer = [];
        // Allocate array for edges so we can draw wireframe
        this.eBuffer = [];
        // Allocate color array
        this.cBuffer = [];
        console.log("Terrain: Allocated buffers");

        this.generateTriangles();
        console.log("Terrain: Generated triangles");

        this.generateLines();
        console.log("Terrain: Generated lines");

        // this.printBuffers();

        // Get extension for 4 byte integer indices for drwElements
        var ext = gl.getExtension('OES_element_index_uint');
        if (ext == null) {
            alert("OES_element_index_uint is unsupported by your browser and terrain generation cannot proceed.");
        }
    }

    /**
     * Generate a random landscape using the diamond-square algorithm.
     * Sets the height of each vertex to a value in the range
     * [minHeight, maxHeight), with the specified roughness factor.
     * A roughness factor near 1.0 will be almost entirely random, while
     * a value near zero will be almost entirely smooth.
     * @param  {float} minHeight minimum final height value
     * @param  {float} maxHeight maximum final height value
     * @param  {float} roughness roughness
     */
    createLandscape(minHeight, maxHeight, roughness) {
        roughness = (roughness > 1.0) ? 1.0 :
                    (roughness < 0.0) ? 0.0 : roughness;

        // initialize corner points to random values
        var v = vec3.create();

        this.getVertex(v, 0, 0);
        v[2] = Math.random();
        // v[2] = 0;
        this.setVertex(v, 0, 0);

        this.getVertex(v, 0, this.div);
        v[2] = Math.random();
        // v[2] = 0;
        this.setVertex(v, 0, this.div);

        this.getVertex(v, this.div, 0);
        v[2] = Math.random();
        // v[2] = 0.25 * Math.random() + 0.75;
        this.setVertex(v, this.div, 0);

        this.getVertex(v, this.div, this.div);
        v[2] = Math.random();
        // v[2] = 0.25 * Math.random() + 0.75;
        this.setVertex(v, this.div, this.div);

        // this.printBuffers()

        // perform diamond-square algorithm "recursivly"
        // (not technically recursion, but the active area
        // of each iteration is reduced as if being called
        // recursivly on a subset of the terrain)
        var iterations = Math.log2(this.div);

        for (var i = 0; i < iterations; i++) {
            // reduce the roughness each iteration, shifting
            // the weighted sum from mostly random in early iterations
            // to mostly averages in later iterations
            var r = Math.pow(roughness, i);

            // each iteration subdivides the terrain in 2, so the
            // step size is div / 2^i
            var step_size = this.div / Math.pow(2, i);

            this.diamond(step_size, r);
            this.square(step_size, r);
        }

        // set colors based on height
        this.setVertexColors();

        // calculate new normals based on heightmap
        this.generateNormals();

        // rescale the landscape from [0,1) to [minHeight,maxHeight)
        var v = vec3.create();
        var dh = maxHeight - minHeight;
        for (var i = 0; i <= this.div; i++) {
            for (var j = 0; j <= this.div; j++) {
                this.getVertex(v, i, j);
                v[2] = minHeight + v[2]*dh;
                this.setVertex(v, i, j);
            }
        }
    }

    /**
     * Iterative diamond step in diamond-square algorithm.
     * Computes the center points of grid squares of size
     * step_size, as if they were the primary unit of the
     * grid (i.e. it ignores the vertices it skips over).
     * Each vertex gets a new height which is the average
     * of the corners and a random value, weighted by r.
     * @param  {[type]} step_size size of sub-tiles to use
     * @param  {[type]} r         roughness weight
     */
    diamond(step_size, r) {
        var v = vec3.create();

        var tl = vec3.create();
        var tr = vec3.create();
        var bl = vec3.create();
        var br = vec3.create();

        var half_step = step_size/2;
        var avg;
        var perturb;

        // iterate over the center vertex of each square in this
        // iteration, setting the height to the weighted average
        // of the four corners and a random perturbation.
        for (var i = half_step; i < this.div; i += step_size) {
            for (var j = half_step; j < this.div; j += step_size) {
                this.getVertex(v, i, j);

                // heights are initially set to negative values,
                // so if the height is non-negative, we already
                // set this point and don't need to do it again.
                if (v[2] < 0) {
                    this.getVertex(tl, i-half_step, j-half_step);
                    this.getVertex(tr, i-half_step, j+half_step);
                    this.getVertex(bl, i+half_step, j-half_step);
                    this.getVertex(br, i+half_step, j+half_step);

                    avg = (tl[2] + tr[2] + bl[2] + br[2]) / 4;
                    perturb = Math.random() - 0.1;

                    v[2] = (r * perturb) + ((1.0 - r) * avg);
                    this.setVertex(v, i, j);
                }
            }
        }
    }

    /**
     * Iterative square step of the diamond-square algorithm.
     * Computes midpoints of tile edges for tiles of size
     * step_size.  New heights are an average of surrounding
     * vertices and a random value, weighted by r.
     * @param  {[type]} step_size size of sub-tiles to use
     * @param  {[type]} r         roughness
     */
    square(step_size, r) {
        var v = vec3.create();
        var temp = vec3.create();

        var half_step = step_size/2;
        var avg;
        var count;
        var perturb;

        // iterate over each potential center vertex, updating
        // the height if the current height is < 0 (uninitialized).
        // Every other potential vertex will be skipped because it
        // was set by a previous diamond step, but this is easier
        // than calculating row shifts.
        for (var i = 0; i <= this.div; i += half_step) {
            for (var j = 0; j <= this.div; j += half_step) {
                this.getVertex(v, i, j);

                if (v[2] < 0) {
                    avg = 0;
                    count = 0;

                    // check the vertex to the left
                    if (i - half_step >= 0) {
                        this.getVertex(temp, i-half_step, j);
                        avg += temp[2];
                        count++;
                    }

                    // check the vertex to the right
                    if (i + half_step <= this.div) {
                        this.getVertex(temp, i+half_step, j);
                        avg += temp[2];
                        count++;
                    }

                    // check the vertex above
                    if (j - half_step >= 0) {
                        this.getVertex(temp, i, j-half_step);
                        avg += temp[2];
                        count++;
                    }

                    // check the vertex below
                    if (j + half_step <= this.div) {
                        this.getVertex(temp, i, j+half_step);
                        avg += temp[2];
                        count++;
                    }

                    // compute the average and add a random perturbation,
                    // creating a weighted sum based on the roughness
                    avg /= count;
                    perturb = Math.random() - 0.1;
                    v[2] = (r * perturb) + ((1.0 - r) * avg);
                    this.setVertex(v, i, j);
                }
            }
        }
    }

    setVertexColors() {
        var waterLine = 0.25;   // transition from water to trees
        var shoreLine = 0.255;   // brief sandy area
        var treeLine = 0.45;    // transition from trees to rock
        var snowLine = 0.60;    // transition from rock to snow

        var waterColor = [ 50/255,  50/255, 150/255, 255/255];  // color from 0 - waterLine
        var sandColor  = [195/255, 178/255, 142/255, 255/255];  // color from waterLine to shoreLine
        var treeColor1 = [ 10/255, 100/255,  10/255, 255/255];  // color at waterLine+
        var treeColor2 = [ 10/255,  40/255,  10/255, 255/255];  // color at treeLine-
        var rockColor1 = [ 40/255,  20/255,   0/255, 255/255];  // color at treeLine+
        var rockColor2 = [ 90/255,  60/255,  30/255, 255/255];  // color at snowLine-
        var snowColor1 = [110/255, 100/255, 100/255, 255/255];  // color at snowLine+
        var snowColor2 = [225/255, 225/255, 225/255, 255/255];  // color at 1

        // color each vertex according to its height
        for (var i = 0; i < this.numVertices; i++) {
            // get the height of the current vertex
            var idx = i*3 + 2;
            var h = this.vBuffer[idx];

            // set the color
            if (h < waterLine) {        // water
                this.vBuffer[idx] = waterLine;
                this.cBuffer.push(...waterColor);
            }
            else if (h < shoreLine) {
                this.vBuffer[idx] = waterLine;
                this.cBuffer.push(...sandColor);
            }
            else if (h < treeLine) {    // trees
                var amt = (h - waterLine) / (treeLine - waterLine);
                var color = this.array_lerp(treeColor1, treeColor2, amt);
                this.cBuffer.push(...color);
            }
            else if (h < snowLine) {    // rock
                var amt = (h - treeLine) / (snowLine - treeLine);
                var color = this.array_lerp(rockColor1, rockColor2, amt);
                this.cBuffer.push(...color);
            }
            else {                      // snow
                var amt = (h - snowLine) / (1 - snowLine);
                var color = this.array_lerp(snowColor1, snowColor2, amt);
                this.cBuffer.push(...color);
            }
        }
    }

    /**
     * Calculate the adjusted normal vectors for each vertex.
     * Use an average of the adjacent face normals for smooth shading.
     */
    generateNormals() {
        var v1 = vec3.create();
        var v2 = vec3.create();
        var v3 = vec3.create();

        var norm = vec3.create();
        var a = vec3.create();
        var b = vec3.create();

        // iterate over each face in the terrain
        for (var i = 0; i < this.numFaces; i++) {
            // get the vertex indices for this face
            var vid1 = this.fBuffer[i*3];
            var vid2 = this.fBuffer[i*3 + 1];
            var vid3 = this.fBuffer[i*3 + 2];

            // get the vertex components for each vertex
            for (var j = 0; j < 3; j++) {
                v1[j] = this.vBuffer[3*vid1 + j];
                v2[j] = this.vBuffer[3*vid2 + j];
                v3[j] = this.vBuffer[3*vid3 + j];
            }

            // calculate the face normal
            vec3.sub(a, v2, v1);
            vec3.sub(b, v3, v1);
            vec3.cross(norm, a, b);

            // if the normal is pointing down, flip it
            if (norm[2] < 0) {
                norm[0] = -norm[0];
                norm[1] = -norm[1];
                norm[2] = -norm[2];
            }

            vec3.normalize(norm, norm);
            // console.log(norm);

            // add the face normal to the each of the vertex normals
            for (var j = 0; j < 3; j++) {
                this.nBuffer[3*vid1 + j] += norm[j];
                this.nBuffer[3*vid2 + j] += norm[j];
                this.nBuffer[3*vid3 + j] += norm[j];
            }
        }

        // normalize each vertex normal
        vec3.forEach(this.nBuffer, 0, 0, 0, vec3.normalize);
    }

    /**
    * Return the x,y,z coordinates of a vertex at location (i,j)
    * @param {Object} v an an array of length 3 holding x,y,z coordinates
    * @param {number} i the ith row of vertices
    * @param {number} j the jth column of vertices
    */
    getVertex(v,i,j) {
        var vid = 3 * (i*(this.div + 1) + j);
        v[0] = this.vBuffer[vid];
        v[1] = this.vBuffer[vid+1];
        v[2] = this.vBuffer[vid+2];
    }

    /**
    * Set the x,y,z coords of a vertex at location(i,j)
    * @param {Object} v an an array of length 3 holding x,y,z coordinates
    * @param {number} i the ith row of vertices
    * @param {number} j the jth column of vertices
    */
    setVertex(v,i,j) {
        var vid = 3 * (i*(this.div + 1) + j);
        this.vBuffer[vid]   = v[0];
        this.vBuffer[vid+1] = v[1];
        this.vBuffer[vid+2] = v[2];
    }

    /**
    * Set the normal vector of a vertex at location(i,j)
    * @param {Object} v an an array of length 3 holding x,y,z components of normal
    * @param {number} i the ith row of vertices
    * @param {number} j the jth column of vertices
    */
    setNormal(v,i,j) {
        var vid = 3 * (i*(this.div + 1) + j);
        this.nBuffer[vid]   = v[0];
        this.nBuffer[vid+1] = v[1];
        this.nBuffer[vid+2] = v[2];
    }


    array_lerp(arr1, arr2, amt) {
        var out = [];
        for (var i = 0; i < arr1.length; i++) {
            out.push(arr1[i] + amt * (arr2[i] - arr1[i]));
        }
        return out;
    }

    /**
    * Send the buffer objects to WebGL for rendering
    */
    loadBuffers() {
        // Specify the vertex coordinates
        this.VertexPositionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.vBuffer), gl.STATIC_DRAW);
        this.VertexPositionBuffer.itemSize = 3;
        this.VertexPositionBuffer.numItems = this.numVertices;
        console.log("Loaded " + this.VertexPositionBuffer.numItems + " vertices");

        // Specify the vertex colors
        this.VertexColorBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexColorBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.cBuffer), gl.STATIC_DRAW);
        this.VertexColorBuffer.itemSize = 4;
        this.VertexColorBuffer.numItems = this.numVertices;
        console.log("Loaded " + this.VertexColorBuffer.numItems + " colors");

        // Specify normals to be able to do lighting calculations
        this.VertexNormalBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexNormalBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.nBuffer), gl.STATIC_DRAW);
        this.VertexNormalBuffer.itemSize = 3;
        this.VertexNormalBuffer.numItems = this.numVertices;
        console.log("Loaded " + this.VertexNormalBuffer.numItems + " normals");

        // Specify faces of the terrain
        this.IndexTriBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexTriBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint32Array(this.fBuffer), gl.STATIC_DRAW);
        this.IndexTriBuffer.itemSize = 1;
        this.IndexTriBuffer.numItems = this.fBuffer.length;
        // this.IndexTriBuffer.itemSize = 3;
        // this.IndexTriBuffer.numItems = this.numFaces;
        console.log("Loaded " + this.IndexTriBuffer.numItems + " triangles");

        //Setup Edges
        this.IndexEdgeBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexEdgeBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint32Array(this.eBuffer), gl.STATIC_DRAW);
        this.IndexEdgeBuffer.itemSize = 1;
        this.IndexEdgeBuffer.numItems = this.eBuffer.length;

        console.log("triangulatedPlane: loadBuffers");

    }

    /**
    * Render the triangles
    */
    drawTriangles() {
        // Bind vertex buffer
        {
            const itemSize = this.VertexPositionBuffer.itemSize;
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);
            gl.vertexAttribPointer(
                shaderProgram.vertexPositionAttribute,
                itemSize,
                type,
                normalize,
                stride,
                offset);
        }

        // Bind color buffer
        {
            const itemSize = this.VertexColorBuffer.itemSize;
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexColorBuffer);
            gl.vertexAttribPointer(
                shaderProgram.vertexColorAttribute,
                itemSize,
                type,
                normalize,
                stride,
                offset);
        }

        // Bind normal buffer
        {
            const itemSize = this.VertexNormalBuffer.itemSize;
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexNormalBuffer);
            gl.vertexAttribPointer(
                shaderProgram.vertexNormalAttribute,
                itemSize,
                type,
                normalize,
                stride,
                offset);
        }

        // Draw
        {
            const itemCount = this.IndexTriBuffer.numItems
            const type = gl.UNSIGNED_INT
            const offset = 0;
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexTriBuffer);
            gl.drawElements(gl.TRIANGLES, itemCount, type, offset);
        }
    }

    /**
    * Render the triangle edges wireframe style
    */
    drawEdges() {
        // Bind vertex buffer
        {
            const itemSize = this.VertexPositionBuffer.itemSize;
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);
            gl.vertexAttribPointer(
                shaderProgram.vertexPositionAttribute,
                itemSize,
                type,
                normalize,
                stride,
                offset);
        }

        // Bind color buffer
        {
            const itemSize = this.VertexColorBuffer.itemSize;
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexColorBuffer);
            gl.vertexAttribPointer(
                shaderProgram.vertexColorAttribute,
                itemSize,
                type,
                normalize,
                stride,
                offset);
        }

        // Bind normal buffer
        {
            const itemSize = this.VertexNormalBuffer.itemSize;
            const type = gl.FLOAT;
            const normalize = false;
            const stride = 0;
            const offset = 0;
            gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexNormalBuffer);
            gl.vertexAttribPointer(
                shaderProgram.vertexNormalAttribute,
                itemSize,
                type,
                normalize,
                stride,
                offset);
        }

        // Draw
        {
            const itemCount = this.IndexEdgeBuffer.numItems
            const type = gl.UNSIGNED_INT
            const offset = 0;
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.IndexEdgeBuffer);
            gl.drawElements(gl.LINES, itemCount, type, offset);
        }
    }

    /**
     * Fill the vertex and buffer arrays
     */
    generateTriangles() {
        var deltaX = (this.maxX - this.minX) / this.div;
        var deltaY = (this.maxY - this.minY) / this.div;

        for (var i = 0; i <= this.div; i++) {
            for (var j = 0; j <= this.div; j++) {
                // create a vertex at this grid position
                this.vBuffer.push(this.minX + deltaX * j);
                this.vBuffer.push(this.minY + deltaY * i);
                this.vBuffer.push(-0.1);

                // set the normal to point up
                this.nBuffer.push(0);
                this.nBuffer.push(0);
                this.nBuffer.push(1);
            }
        }

        // fill the face buffer with vertices from the grid
        for (var i = 0; i < this.div; i++) {
            for (var j = 0; j < this.div; j++) {
                var vid = i*(this.div + 1) + j;
                this.fBuffer.push(vid);                     // reference vertex
                this.fBuffer.push(vid + 1);                 // vertex to the right
                this.fBuffer.push(vid + this.div + 1);      // vertex above

                this.fBuffer.push(vid + 1);                 // vertex to the right
                this.fBuffer.push(vid + this.div + 1);      // vertex above
                this.fBuffer.push(vid + this.div + 2);      // vertex above right
            }
        }
    }

    /**
     * Print vertices and triangles to console for debugging
     */
    printBuffers() {
        for(var i = 0; i < this.numVertices; i++) {
            console.log("v %.2f %.2f %.2f", this.vBuffer[i*3],
                                      this.vBuffer[i*3 + 1],
                                      this.vBuffer[i*3 + 2]);
        }

        for(var i = 0; i < this.numFaces; i++) {
            console.log("f %d %d %d", this.fBuffer[i*3],
                              this.fBuffer[i*3 + 1],
                              this.fBuffer[i*3 + 2]);
        }
    }

    /**
     * Generates line values from faces in faceArray
     * to enable wireframe rendering
     */
    generateLines() {
        var numTris = this.fBuffer.length / 3;
        for(var f = 0; f < numTris; f++) {
            var fid = f*3;
            this.eBuffer.push(this.fBuffer[fid]);
            this.eBuffer.push(this.fBuffer[fid+1]);

            this.eBuffer.push(this.fBuffer[fid+1]);
            this.eBuffer.push(this.fBuffer[fid+2]);

            this.eBuffer.push(this.fBuffer[fid+2]);
            this.eBuffer.push(this.fBuffer[fid]);
        }
    }
}
